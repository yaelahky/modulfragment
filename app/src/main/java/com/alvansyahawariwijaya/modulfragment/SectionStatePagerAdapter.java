package com.alvansyahawariwijaya.modulfragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

public class SectionStatePagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFagmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public SectionStatePagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    void addFragment(Fragment fragment, String judul) {
        mFagmentList.add(fragment);
        mFragmentTitleList.add(judul);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFagmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFagmentList.size();
    }
}
